#!/usr/bin/python2

try:

    from tkinter import * #python3
except ImportError: 
    from Tkinter import * #python2

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
pwm = GPIO.PWM(18, 100)
pwm.start(5)

class App:
	
    def __init__(self, master):
        frame = Frame(master)
        frame.pack()
        scale = Scale(frame, from_=0, to=220, 
              orient=HORIZONTAL, command=self.update)
        scale.grid(row=2)


    def update(self, angle):
        duty = float(angle) / 10.0 + 2.5
        pwm.ChangeDutyCycle(duty)

root = Tk()
root.wm_title('RPi Polarotor Control')
app = App(root)
root.geometry("300x50+0+0")
root.mainloop()
